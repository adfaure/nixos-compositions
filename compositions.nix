{
  oar-evolving = import ./oar-evolving/composition.nix;
  oar-power-token = import ./oar-power-token/composition.nix;
  oar-plugins = import ./oar-plugins/composition.nix;
}
