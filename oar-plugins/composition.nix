{ pkgs, modulesPath, nur, helpers, flavour, ... }: {
  dockerPorts.frontend = [ "8443:443" "8000:80" ];
  nodes =
    let
      nodes_number = 4;
      commonConfig = import ./../lib/oar_config.nix { inherit pkgs modulesPath nur flavour; };
      node = { ... }: {
        imports = [ commonConfig ];
        services.oar.node = { enable = true; };
      };
    in {
      frontend = { ... }: {
        imports = [ commonConfig ];
        # services.phpfpm.phpPackage = pkgs.php74;
        services.oar.client.enable = true;
        services.oar.web.enable = true;
        services.oar.web.drawgantt.enable = true;
        services.oar.web.monika.enable = true;
      };
      server = { ... }: {
        imports = [ commonConfig ];
        services.oar.plugins = [ pkgs.nur.repos.kapack.oar3-plugins ];
        services.oar.server.enable = true;
        services.oar.dbserver.enable = true;
      };
    } // helpers.makeMany node "node" nodes_number;

  testScript = ''
    frontend.succeed("true")
  '';
}
