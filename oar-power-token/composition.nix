{ pkgs, modulesPath, nur, helpers, flavour, ... }: {
  dockerPorts.frontend = [ "8443:443" "8000:80" ];
  nodes =
    let
      commonConfig = import ./../lib/oar_config.nix { inherit pkgs modulesPath nur flavour; };
    in {
      frontend = { ... }: {
        imports = [ commonConfig ];
        # services.phpfpm.phpPackage = pkgs.php74;
        services.oar.client.enable = true;
        services.oar.web.enable = true;
        services.oar.web.drawgantt.enable = true;
      };
      server = { ... }: {
        imports = [ commonConfig ];
        services.oar.server.enable = true;
        services.oar.dbserver.enable = true;

        systemd.services.oar-post-init = {
          after = [ "oar-db-init.service" ];
          wantedBy = [ "multi-user.target" ];
          serviceConfig.Type = "oneshot";

          script = ''
            ${pkgs.nur.repos.kapack.oar}/bin/.oarproperty -a token
            nodes_number=$(${pkgs.jq}/bin/jq -r '[.nodes[] | select(contains("node"))]| length' /etc/nxc/deployment.json)
            for ((token=1;token<=''$nodes_number; token++)); do
              ${pkgs.nur.repos.kapack.oar}/bin/.oarnodesetting -a -p type=powertoken -p token=$token
              wait
            done
          '';
        };

      };
      node = { ... }: {
        imports = [ commonConfig ];
        services.oar.node = { enable = true; };
      };
    };

  rolesDistribution = { node = 2; };

  testScript = ''
    frontend.succeed("true")
  '';
}
