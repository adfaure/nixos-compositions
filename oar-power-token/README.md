# OAR Power token

Proof-of-concept of power tokens using OAR that can be deployed with nixos-compose.
Power-token are virtual resources (not attached to any hardware) that enable to have power-capping capabilities with OAR3.

## Deploy on your laptop

To deploy on your laptop using nixos compose, you can type the following commands:

```bash
# Create the composition using the laptop configuration (see setup.toml)
nxc build -f docker -s laptop

# Start the composition
nxc start -f docker -s laptop
```

## Deploy on G5K

You can follow this [tutorial](https://nixos-compose.gitlabpages.inria.fr/nixos-compose/quick-start.html#deployment) to deploy on G5K.
To sum up the important steps:

To build on G5K, you can first take a node to build the composition:

```bash
oarsub -I

cd oar-power-token
nxc build -f g5k-ramdisk -s g5k
```

- Install nxc with this [tutorial page](https://nixos-compose.gitlabpages.inria.fr/tuto-nxc/install_nxc/without_nix.html)

```bash
# cd into your composition repository
cd oar-power-token
# create your job reservation
export $(oarsub -l nodes=1,walltime=0:30 "$(nxc helper g5k_script) 30m" | grep OAR_JOB_ID)

# Once the job is running, the file is created OAR.$OAR_JOB_ID.stdout
```

To start the composition on the allocated node, use the command:

```bash
nxc start -f g5k-ramdisk -m OAR.$OAR_JOB_ID.stdout
```

## Use the OAR cluster with the power tokens

Once the cluster deployed, and running, you can connect to the server and display the resources (connect with `nxc connect server`). Type the command `oarnodes` to see the virtual resources (among all other resources):

```
network_address: server
resource_id: 9
state: Alive
properties: type=powertoken, network_address=server, cpuset=0, besteffort=YES, deploy=NO, desktop_computing=NO, available_upto=2147483647, last_available_upto=, drain=NO, token=1
network_address: server
resource_id: 10
state: Alive
properties: type=powertoken, network_address=server, cpuset=0, besteffort=YES, deploy=NO, desktop_computing=NO, available_upto=2147483647, last_available_upto=, drain=NO, token=2
```

To run a job that needs power token you can use the following command:

```bash
oarsub -I -l "nodes=1+{type='powertoken'}/token=1"
```

## Access the web interface (drawgantt)

To access to the drawgantt interface, g5k gives the possibility to access to nodes via HTTP(s).
The procedure is explained in this [wiki page](https://www.grid5000.fr/w/HTTP/HTTPs_access).
Long story short, go to your web browser to the url `https://mynode.mysite.http.proxy.grid5000.fr/`.

For instance, on a dahu-node in Grenoble use `https://dahu-18.grenoble.http.proxy.grid5000.fr/drawgantt`.

This command can be used to find the node associated to your deployement:

```bash
oarnodes --sql "ip = '$(cat composition::g5k-ramdisk.json | jq -r '.deployment | to_entries[] | select(.value.role=="frontend") | .key')' limit 1" | grep network_address:
network_address: dahu-18.grenoble.grid5000.fr
```
